using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTowardsTarget : MonoBehaviour
{
    public Rigidbody2D target;

    private Vector2 target_scale = Vector2.one;
    private Vector3 target_rotation = Vector3.zero;

    private void Update()
    {
        //transform.LookAt(target.transform);

        Vector2 diff = target.position - (Vector2)transform.position;

        target_rotation.z = Mathf.Atan2(-diff.x, diff.y) * Mathf.Rad2Deg;
        transform.eulerAngles = target_rotation;

        target_scale.y = diff.magnitude;

        transform.localScale = target_scale;
    }
}
