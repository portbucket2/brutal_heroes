using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Avatar : MonoBehaviour
{
    public float max_health;
    public HealthBar health_bar;

    public bool is_dead = false;
    public bool is_almost_dead = false;
    private bool about_to_finish = false;

    private GameManager game_manager;

    private void Awake()
    {
        game_manager = FindObjectOfType<GameManager>();
    }

    public void ReceiveDamage(float damage)
    {
        if (is_dead || is_almost_dead || about_to_finish)
        {
            return;
        }
        health_bar.UpdateHealth(-damage);

        if (health_bar.GetHealthPercentage() <= 0f)
        {
            //is_dead = true;
            if (!about_to_finish)
                {
                    about_to_finish = true;
                    Enemy enemy = GetComponent<Enemy>();
                    if (enemy != null)
                    {
                        Invoke("AlmostDead", 1f);
                        enemy.MoveRight();
                        enemy.MoveRight();
                        enemy.MoveRight();
                    }
                    else
                    {
                        is_dead = true;
                        game_manager.Failed();
                    }
                }


            //if (game_manager.slow_down_counter > 0)
            //{
            //    game_manager.slow_down_counter--;
            //}
        }
    }

    private void AlmostDead()
    {
        is_almost_dead = true;
        game_manager.FinishHim();
    }

    public void Finisher()
    {
        is_dead = true;
        is_almost_dead = false;

        if (Time.timeScale >= 0.9)
        {
            Time.timeScale = 0.3f;
            Invoke("DefaultSpeed", 0.8f);
        }
    }

    public float GetHealthPercentage()
    {
        return health_bar.GetHealthPercentage();
    }

    private void DefaultSpeed()
    {
        Time.timeScale = 1f;
        game_manager.LevelEnd();
    }
}
