using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D.IK;

public class PlayerController : MonoBehaviour
{
    public float movement_speed;

    public Animator avatar_ik_anim;

    private Rigidbody2D player_rb;

    public float joy_max_distance;
    private Vector2 joystick_right_rest_pos;
    public GameObject joystick_right_base;
    public GameObject joystick_right_handle;
    private Vector2 touch_diff_right;

    private Vector2 joystick_left_rest_pos;
    public GameObject joystick_left_base;
    public GameObject joystick_left_handle;
    private Vector2 touch_diff_left;

    public float joy_diff_multiplier;
    public float joy_diff_multiplier_head;
    public GameObject right_shoulder;
    public GameObject left_shoulder;

    public GameObject forearm_back_target;
    private Vector2 forearm_back_rest;
    public GameObject head_target;
    private Vector2 head_rest;
    public GameObject forearm_front_target;
    private Vector2 forearm_front_rest;

    public float reset_speed;


    public List<Muscle> muscles;

    public List<PositionFollower> positionFollow;

    public Rigidbody2D root;


    private Vector2 intended_position = new Vector2(-2, 0);

    private Vector2 head_movement_offset = Vector2.zero;

    private float double_tap_right_timer = 0f;
    private float double_tap_left_timer = 0f;

    private int right_touch_id = -1;
    private int left_touch_id = -1;

    public Text[] debug_text;

    private Avatar avatar;
    //private Enemy enemy;
    public Rigidbody2D enemy_spine;

    private GameManager game_manager;

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();

        //enemy = FindObjectOfType<Enemy>();
        avatar = GetComponent<Avatar>();

        avatar.health_bar.max_health = avatar.max_health;
        avatar.health_bar.Initiate();

        intended_position = transform.position;

        player_rb = GetComponent<Rigidbody2D>();

        Vector3 offset = Vector3.zero;
        offset.x = transform.position.x;
        
        forearm_front_rest = forearm_front_target.transform.position - offset;
        forearm_back_rest = forearm_back_target.transform.position - offset;
        head_rest = head_target.transform.position - offset;

        joystick_right_rest_pos = joystick_right_base.transform.position;
        joystick_left_rest_pos = joystick_left_base.transform.position;

        foreach (Muscle muscle in muscles)
        {
            muscle.hinge = muscle.rb.GetComponent<HingeJoint2D>();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.hinge = follower.rb.GetComponent<HingeJoint2D>();
        }
    }

    bool right = false;
    private void Update()
    {
        debug_text[0].text = ((int)(1f / Time.unscaledDeltaTime)).ToString();

        if (double_tap_left_timer > 0)
        {
            double_tap_left_timer -= Time.deltaTime;
        }

        if (double_tap_right_timer > 0)
        {
            double_tap_right_timer -= Time.deltaTime;
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x > Screen.width / 2)
            {
                right = true;

                joystick_right_base.SetActive(true);
                joystick_right_base.transform.position = Input.mousePosition;


                if ((intended_position.x <= enemy_spine.position.x - 2) || intended_position.x < game_manager.map_right_limit)
                //if (transform.position.x < 10)
                {
                    if (double_tap_right_timer > 0)
                    {
                        //head_movement_offset = Vector2.right * 2;
                        intended_position += Vector2.right * 2;
                        avatar_ik_anim.Play("walk_forward", -1, 0);
                    }
                    else
                    {
                        double_tap_right_timer = 0.5f;
                    }
                }
            }
            else
            {
                right = false;

                joystick_left_base.SetActive(true);
                joystick_left_base.transform.position = Input.mousePosition;

                if (intended_position.x > game_manager.map_left_limit)
                {
                    if (double_tap_left_timer > 0)
                    {
                        //head_movement_offset = Vector2.left * 2;
                        intended_position += Vector2.left * 2;
                        avatar_ik_anim.Play("walk_backward", -1, 0);
                    }
                    else
                    {
                        double_tap_left_timer = 0.5f;
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            touch_diff_right = Vector2.zero;
            touch_diff_left = Vector2.zero;
            joystick_right_handle.transform.localPosition = Vector2.zero;
            joystick_left_handle.transform.localPosition = Vector2.zero;

            //joystick_right_base.SetActive(false);
            //joystick_left_base.SetActive(false);

            joystick_right_base.transform.position = joystick_right_rest_pos;
            joystick_left_base.transform.position = joystick_left_rest_pos;

            head_movement_offset = Vector2.zero;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
            Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

            if (right)
            {
                touch_diff_right = Input.mousePosition - joystick_right_base.transform.position;

                if (touch_diff_right.magnitude < joy_max_distance)
                {
                    joystick_right_handle.transform.position = Input.mousePosition;
                }
                else
                {
                    joystick_right_handle.transform.position = (Vector2)joystick_right_base.transform.position + touch_diff_right.normalized * joy_max_distance;
                    joystick_right_base.transform.position = Vector2.Lerp(joystick_right_base.transform.position, Input.mousePosition, Time.deltaTime * 2f);
                }

                forearm_front_target.transform.position = (Vector2)left_shoulder.transform.position + joy_diff_right * joy_diff_multiplier + Vector2.right * 1f;
            }
            else
            {
                touch_diff_left = Input.mousePosition - joystick_left_base.transform.position;

                if (touch_diff_left.magnitude < joy_max_distance)
                {
                    joystick_left_handle.transform.position = Input.mousePosition;
                }
                else
                {
                    joystick_left_handle.transform.position = (Vector2)joystick_left_base.transform.position + touch_diff_left.normalized * joy_max_distance;
                    joystick_left_base.transform.position = Vector2.Lerp(joystick_left_base.transform.position, Input.mousePosition, Time.deltaTime * 2f);
                }

                forearm_back_target.transform.position = (Vector2)right_shoulder.transform.position + joy_diff_left * joy_diff_multiplier + Vector2.right * 1f;
            }

            head_target.transform.position = (Vector2)transform.position + (joy_diff_right + joy_diff_left) / 1 * joy_diff_multiplier_head + Vector2.up * 3.6f + head_movement_offset;
            //head_target.transform.position = forearm_front_target.transform.position + (forearm_back_target.transform.position - forearm_front_target.transform.position) * 0.5f;
        }
        else
        {
            Vector2 offset = Vector2.zero;
            offset.x = transform.position.x;

            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
            head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        }
#else

        int touch_idx = 0;

        while (touch_idx < Input.touchCount)
        {

            Touch t = Input.GetTouch(touch_idx);

            //if (touch_idx < debug_text.Length)
            //{
            //    debug_text[touch_idx].text = t.position.ToString();
            //}

            if (t.phase == TouchPhase.Began)
            {
                if (t.position.x > Screen.width / 2)
                {
                    joystick_right_base.transform.position = t.position;
                    right_touch_id = t.fingerId;

                    if ((intended_position.x <= enemy_spine.position.x - 2) || intended_position.x < game_manager.map_right_limit)
                    {
                        if (double_tap_right_timer > 0)
                        {
                            //head_movement_offset = Vector2.right * 2;
                            intended_position += Vector2.right * 2;
                            avatar_ik_anim.Play("walk_forward", -1, 0);
                        }
                        else
                        {
                            double_tap_right_timer = 0.5f;
                        }
                    }
                }
                else
                {
                    joystick_left_base.transform.position = t.position;
                    left_touch_id = t.fingerId;

                    if (intended_position.x > game_manager.map_left_limit)
                    {
                        if (double_tap_left_timer > 0)
                        {
                            //head_movement_offset = Vector2.left * 2;
                            intended_position += Vector2.left * 2;
                            avatar_ik_anim.Play("walk_backward", -1, 0);
                        }
                        else
                        {
                            double_tap_left_timer = 0.5f;
                        }
                    }
                }
            }
            else if (t.phase == TouchPhase.Ended)
            {
                if (t.fingerId == right_touch_id)
                {
                    touch_diff_right = Vector2.zero;
                    joystick_right_handle.transform.localPosition = Vector2.zero;
                    joystick_right_base.transform.position = joystick_right_rest_pos;

                    right_touch_id = -1;
                }

                if (t.fingerId == left_touch_id)
                {
                    touch_diff_left = Vector2.zero;
                    joystick_left_handle.transform.localPosition = Vector2.zero;
                    joystick_left_base.transform.position = joystick_left_rest_pos;

                    left_touch_id = -1;
                }

                head_movement_offset = Vector2.zero;
            }
            else if (t.phase == TouchPhase.Moved)
            {
                Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
                Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

                if (t.fingerId == right_touch_id)
                {
                    touch_diff_right = t.position - (Vector2)joystick_right_base.transform.position;

                    if (touch_diff_right.magnitude < joy_max_distance)
                    {
                        joystick_right_handle.transform.position = t.position;
                    }
                    else
                    {
                        joystick_right_handle.transform.position = (Vector2)joystick_right_base.transform.position + touch_diff_right.normalized * joy_max_distance;
                        joystick_right_base.transform.position = Vector2.Lerp(joystick_right_base.transform.position, t.position, Time.deltaTime * 2f);
                    }

                    forearm_front_target.transform.position = (Vector2)left_shoulder.transform.position + joy_diff_right * joy_diff_multiplier + Vector2.right * 1f;
                }


                if(t.fingerId == left_touch_id)
                {
                    touch_diff_left = t.position - (Vector2)joystick_left_base.transform.position;

                    if (touch_diff_left.magnitude < joy_max_distance)
                    {
                        joystick_left_handle.transform.position = t.position;
                    }
                    else
                    {
                        joystick_left_handle.transform.position = (Vector2)joystick_left_base.transform.position + touch_diff_left.normalized * joy_max_distance;
                        joystick_left_base.transform.position = Vector2.Lerp(joystick_left_base.transform.position, t.position, Time.deltaTime * 2f);
                    }

                    forearm_back_target.transform.position = (Vector2)right_shoulder.transform.position + joy_diff_left * joy_diff_multiplier + Vector2.right * 1f;
                }

                head_target.transform.position = (Vector2)transform.position + (joy_diff_right + joy_diff_left) / 1 * joy_diff_multiplier_head + Vector2.up * 3.6f + head_movement_offset;
                //head_target.transform.position = forearm_front_target.transform.position + (forearm_back_target.transform.position - forearm_front_target.transform.position) * 0.5f;
            
        }

            touch_idx++;
        }


        Vector2 offset = Vector2.zero;
        offset.x = transform.position.x;

        if(right_touch_id == -1)
        {
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
        }
        if(left_touch_id == -1)
        {
            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
        }

        if(right_touch_id == -1 && left_touch_id == -1)
        {
            head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        }

#endif


        //avatar_ik.transform.position -= Vector3.left * Time.deltaTime * 2f;

        //Vector2 t = (Vector2)root.transform.position + Vector2.right * Time.deltaTime * 2f;

        //root.MovePosition(t);
        //root.AddForce(Vector2.right * 500f);

        head_movement_offset = Vector2.Lerp(head_movement_offset, Vector2.zero, Time.deltaTime * 8);

        if (intended_position.x > game_manager.map_right_limit)
        {
            intended_position.x = game_manager.map_right_limit;
        }
        if (intended_position.x < game_manager.map_left_limit)
        {
            intended_position.x = game_manager.map_left_limit;
        }
    }

    private void FixedUpdate()
    {
        //player_rb.MovePosition(Vector2.right);
        if (avatar.is_dead)
        {
            return;
        }

        foreach (Muscle muscle in muscles)
        {
            muscle.ActivateMuscle();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }

        //transform.position = Vector2.Lerp(transform.position, intended_position, Time.deltaTime * movement_speed);
        player_rb.MovePosition(Vector2.Lerp(player_rb.position, intended_position, Time.deltaTime * movement_speed));
    }

}
[System.Serializable]
public class Muscle
{
    public Rigidbody2D rb;
    public HingeJoint2D hinge;
    //public float restRotation;
    public Rigidbody2D targetBody;
    public float force;

    public void ActivateMuscle()
    {
        if (rb.sharedMaterial == null)
        {
            rb.MoveRotation(Mathf.LerpAngle(rb.rotation, targetBody.rotation, force * Time.deltaTime));
        }
        //rb.MovePosition(Vector2.Lerp(rb.position, targetBody.position, force * Time.deltaTime));

    }
}

[System.Serializable] 
public class PositionFollower
{
    public Rigidbody2D rb;
    public Rigidbody2D targetBody;
    public HingeJoint2D hinge;

    public float force;

    public void FollowPosition()
    {
        if (rb.sharedMaterial == null)
        {
            rb.MovePosition(Vector2.Lerp(rb.position, targetBody.position, force * Time.deltaTime));
        }
    }
}